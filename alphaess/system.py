#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright © 2019 Philip Withnall
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this library; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA


import datetime


__all__ = ['System']


class System:
    def __init__(self, session, serial_number, json=None):
        self.__session = session
        self.__serial_number = serial_number
        self.__got_detail = False
        self.__got_status = False

        if json:
            self.__id = json['Id']
            self.__serial_number = json['Sn']
            self.__system_model = json['SystemModel']
            self.__labelled_capacity = json['Cobat']
            self.__usable_capacity = json['UsableCapacity']
            self.__battery_model = json['Mbat']
            self.__inverter_power = json['Poinv']
            self.__pv_power = json['Popv']
            self.__remark = json['Remark']
            self.__ems_version = json['EmsVersion']
            self.__bms_version = json['BmsVersion']
            self.__inverter_version = json['InvVersion']
            self.__inverter_model = json['InvModel']
            self.__meter_model = json['MeterModel']
            self.__meter_phase = json['MeterPhase']
            self.__network_status = json['NetWorkStatus']
            self.__state = json['State']

            # FIXME: Unknown fields
            # 'Solution': '1',
            # 'SetFeed': 100,
            # 'EndUser': '',
            # 'TransFrequency': 10,
            # 'ListWeatherForecast': None


    @property
    def id(self):
        return self.__id


    @property
    def serial_number(self):
        return self.__serial_number


    @property
    def system_model(self):
        return self.__system_model


    @property
    def labelled_capacity(self):
        return self.__labelled_capacity


    @property
    def usable_capacity(self):
        return self.__usable_capacity


    @property
    def battery_model(self):
        return self.__battery_model


    @property
    def inverter_model(self):
        self.__ensure_status()
        return self.__inverter_model


    @property
    def state(self):
        self.__ensure_status()
        return self.__state


    @property
    def installation_cost(self):
        self.__ensure_detail()
        return self.__installation_cost


    @property
    def export_prices(self):
        self.__ensure_detail()
        return self.__export_prices


    @property
    def import_prices(self):
        self.__ensure_detail()
        return self.__import_prices


    # TODO: Getters for other properties


    def __ensure_detail(self):
        if self.__got_detail:
            return
        uri = 'http://api.alphaess.com/ras/v3/GetSystemDetail'
        response = self.__session.post_request(uri, {
            'sn': self.__serial_number,
        })

        # FIXME: Quite a lot missed out here. import_prices and export_prices
        # will eventually be lists of prices and time ranges they apply in.
        self.__installation_cost = (response['Result']['moneytype'], response['Result']['InputCost'])
        self.__export_prices = [(response['Result']['moneytype'], response['Result']['Outputcost'], 0, 23*60+59)]
        self.__import_prices = [(response['Result']['moneytype'], response['Result']['SalePrice0'], 0, 23*60+59)]


    def __ensure_status(self):
        if self.__got_status:
            return
        uri = 'http://api.alphaess.com/ras/v3/GetSystemStatus'
        response = self.__session.post_request(uri, {
            'sn': self.__serial_number,
        })

        # Not sure why the results are in an array.
        self.__inverter_model = response['Result'][0]['Minv']
        self.__network_status = response['Result'][0]['NetWorkStatus']
        self.__state = response['Result'][0]['State']

        # FIXME: Unknown fields
        # 'TransFrequency': 10,


    def get_energy_summary(self, date):
        uri = 'http://api.alphaess.com/ras/v3/GetEnergySummary'
        return self.__session.post_request(uri, {
            'Sn': self.__serial_number,
            'TheDate': date.isoformat(),
        })


    def get_last_power_data(self):
        uri = 'http://api.alphaess.com/ras/v3/GetLastPowerData'
        return self.__session.post_request(uri, {
            'Sn': self.__serial_number,
        })


    def get_energy_data(self, start_date, end_date, statistics_by='day'):
        uri = 'http://api.alphaess.com/ras/v3/GetEnergeData'
        response = self.__session.post_request(uri, {
            'sn': self.__serial_number,
            'statisticsby': statistics_by,
            'username': '',
            'date_start': start_date.isoformat(),
            'date_end': end_date.isoformat(),
        })

        # FIXME: We don’t support other grouping levels yet.
        assert statistics_by == 'day'

        # FIXME: We don’t expose the summaries
        # response['Result']['EselfConsumption'] or
        # response['Result']['EselfSufficiency'].

        data = response['Result']
        retval = list()
        assert len(data['EGrid2Load']) == len(data['EGridCharge'])
        assert len(data['EGridCharge']) == len(data['Ebat'])
        assert len(data['Ebat']) == len(data['Echarge'])
        assert len(data['Echarge']) == len(data['Eeff'])
        assert len(data['Eeff']) == len(data['Einput'])
        assert len(data['Einput']) == len(data['Eload'])
        assert len(data['Eload']) == len(data['Eout'])
        assert len(data['Eout']) == len(data['Epv2load'])
        assert len(data['Epv2load']) == len(data['EpvT'])
        assert len(data['EpvT']) == len(data['Timeline'])

        for i in range(len(data['EGrid2Load'])):
            date = datetime.date.fromisoformat(data['Timeline'][i])
            retval.append((date.isoformat(), data['EGrid2Load'][i],
                           data['EGridCharge'][i], data['Ebat'][i],
                           data['Echarge'][i], data['Eeff'][i],
                           data['Einput'][i], data['Eload'][i],
                           data['Eout'][i], data['Epv2load'][i],
                           data['EpvT'][i]))

        return retval


    def get_power_data(self, date):
        uri = 'http://api.alphaess.com/ras/v3/GetPowerData'
        response = self.__session.post_request(uri, {
            'sn': self.__serial_number,
            'date': date.isoformat(),
            'username': '',
        })

        # FIXME: Don’t know what response['Result']['EFeedIn'],
        # response['Result']['EGridCharge'], response['Result']['ELoad'],
        # response['Result']['EBat'], response['Result']['ECharge'] or
        # response['Result']['EPvTotal'] mean.

        data = response['Result']
        retval = list()
        assert len(data['Cbat']) == len(data['FeedIn'])
        assert len(data['FeedIn']) == len(data['GridCharge'])
        assert len(data['GridCharge']) == len(data['UsePower'])
        assert len(data['UsePower']) == len(data['Time'])

        for i in range(len(data['Cbat'])):
            # Skip the midnight reading as it’s duplicated the next day.
            if data['Time'][i] == '24:00':
                continue

            iso_str = '{}T{}'.format(date.isoformat(), data['Time'][i].zfill(5))
            date_time = datetime.datetime.fromisoformat(iso_str)
            retval.append((date_time.isoformat(), data['Cbat'][i],
                           data['FeedIn'][i], data['GridCharge'][i],
                           data['UsePower'][i]))

        return retval


    def get_profit_data(self, start_date, end_date, statistics_by='day'):
        uri = 'http://api.alphaess.com/ras/v3/GetProfitData'
        return self.__session.post_request(uri, {
            'sn': self.__serial_number,
            'statisticsby': statistics_by,
            'username': '',
            'date_start': start_date.isoformat(),
            'date_end': end_date.isoformat(),
        })
