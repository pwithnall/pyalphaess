#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright © 2019 Philip Withnall
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this library; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA


from .system import System

import base64
from Crypto.Cipher import AES
import hashlib
import requests
import time


__all__ = ['Session']


class Session:
    def __init__(self):
        self.__token = None
        # FIXME: their TLS certificate for api.alphaess.com is still invalid
        self.__verify = False


    def __hash_password(self, plaintext_password, username):
        # From EncryptPassword() in the APK.
        username_sha256 = hashlib.sha256(username)
        username_md5 = hashlib.md5(username)
        aes = AES.new(key=bytes(username_sha256.digest()), mode=AES.MODE_CBC, IV=bytes(username_md5.digest()))

        # Pad using PKCS#7
        length = 16 - (len(plaintext_password) % 16)
        plaintext_password += bytes([length])*length

        return str(base64.b64encode(aes.encrypt(plaintext_password)), 'utf-8')


    def __sign_json(self, json):
        # From GetSign2() in the APK.
        api_account = 'android'
        api_secret_key = 'ALPHAESSWEBAPI201510'
        timestamp = int(time.time())

        json['api_account'] = api_account
        json['timestamp'] = str(timestamp)
        json['secretkey'] = api_secret_key

        plaintext = ''
        for key in sorted(json.keys(), key=str.casefold):
            plaintext += '{}={}'.format(key, json[key])

        plaintext_md5 = hashlib.md5(bytes(plaintext, 'utf-8'))

        json['sign'] = plaintext_md5.hexdigest()
        del(json['secretkey'])

        return json


    def __assert_logged_in(self):
        if self.__token is None:
            raise Exception('Not logged in')


    def login(self, username, password):
        url = 'http://api.alphaess.com/ras/v3/Login'

        json = self.__sign_json({
            'username': username,
            'password': self.__hash_password(bytes(password, 'utf-8'), bytes(username, 'utf-8')),
        })
        response = requests.post(url, json=json, verify=self.__verify)
        response.raise_for_status()
        response_json = response.json()
        if response_json['ReturnCode'] != 0:
            raise Exception('Login failed (return code {})'.format(response_json['ReturnCode']))
        self.__token = response_json['Token']


    def post_request(self, uri, params, with_token=True):
        self.__assert_logged_in()

        if with_token:
            params = { **params, **{
                'Token': self.__token,
            }}

        json = self.__sign_json(params)
        response = requests.post(uri, json=json, verify=self.__verify)
        response.raise_for_status()
        response_json = response.json()
        if response_json['ReturnCode'] != 0:
            raise Exception('Request failed (return code {})'.format(response_json['ReturnCode']))
        return response_json


    def get_message_list(self, only_unread=True, page_index=1, page_size=5):
        uri = 'http://api.alphaess.com/ras/v3/GetMsgList'
        return self.post_request(uri, {
            'pageindex': page_index,
            'pagesize': page_size,
            'onlyUnread': 1 if only_unread else 0,
        })


    def get_system_list(self, page_index=1, page_size=5):
        uri = 'http://api.alphaess.com/ras/v3/GetSystemList'
        response = self.post_request(uri, {
            'pageindex': page_index,
            'pagesize': page_size,
        })

        # FIXME: We don’t currently support pagination here.
        assert not response['Result']['HasNextPage']
        assert not response['Result']['HasPreviousPage']

        system_list = response['Result']['Items']
        assert response['Result']['TotalCount'] == len(system_list)

        return map(lambda json : System(session=self,
                                        serial_number=json['Sn'],
                                        json=json), system_list)


    def get_user_info(self, username):
        uri = 'http://api.alphaess.com/ras/v3/GetUserinfo'
        return self.post_request(uri, {
            'username': username,
        })


    def get_complaints_list(self, page_index=1, page_size=5):
        uri = 'http://api.alphaess.com/ras/v3/GetComplaintsList'
        return self.post_request(uri, {
            'pageindex': 1,
            'pagesize': 20,
        })


    def get_company_contacts(self):
        uri = 'http://api.alphaess.com/ras/v3/GetCompanyContacts'
        return self.post_request(uri, {
            'flag': 1,
        }, with_token=False)
