#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright © 2019 Philip Withnall
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this library; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA


import alphaess
import argparse
import configparser
import csv
import datetime


def main():
    end_date = datetime.date.today()
    start_date = end_date - datetime.timedelta(days=7)
    username = None
    password = None

    try:
        config = configparser.ConfigParser()
        config.read('credentials.ini')
        username = config['alphaess.com']['username']
        password = config['alphaess.com']['password']
    except:
        pass

    parser = argparse.ArgumentParser(description='Dump alphaess battery data')
    parser.add_argument('--username', '-u', type=str, default=username,
                        required=(username is None),
                        help='account username (default: loaded from credentials.ini)')
    parser.add_argument('--password', '-p', type=str, default=password,
                        required=(password is None),
                        help='account password (default: loaded from credentials.ini)')
    parser.add_argument('-s', '--start-date',
                        help='start date for dump, inclusive (format: YYYY-MM-DD, default: 7 days ago)',
                        type=datetime.date.fromisoformat, default=start_date)
    parser.add_argument('-e', '--end-date',
                        help='end date for dump, inclusive (format: YYYY-MM-DD, default: today)',
                        type=datetime.date.fromisoformat, default=end_date)

    args = parser.parse_args()

    # Create a session and dump everything
    session = alphaess.Session()
    session.login(args.username, args.password)

    print(session.get_message_list())
    print(session.get_user_info(args.username))
    print(session.get_complaints_list())
    print(session.get_company_contacts())

    system_list = session.get_system_list()

    for system in system_list:
        print('System {} ({}, {}, {}W)\n'.format(system.serial_number,
                                                 system.inverter_model,
                                                 system.battery_model,
                                                 system.labelled_capacity))
        print(' • Installation cost: {}{}\n'.format(system.installation_cost[0],
                                                    system.installation_cost[1]))
        print(' • State: {}\n'.format(system.state))

        print(system.get_energy_summary(args.end_date))
        print(system.get_last_power_data())
        print(system.get_profit_data(args.start_date, args.end_date))

        with open('power-data.csv', 'w', newline='') as power_data_file:
            power_data = system.get_power_data(args.end_date)

            writer = csv.writer(power_data_file)
            writer.writerow(['Time', 'Battery charge (%)', 'Feed in (W)', 'Grid consumption (W)', 'Load (W)'])
            for row in power_data:
                writer.writerow(row)

        with open('energy-data.csv', 'w', newline='') as energy_data_file:
            energy_data = system.get_energy_data(args.start_date, args.end_date)

            writer = csv.writer(energy_data_file)
            writer.writerow(['Date', 'Grid to load (kWh)', 'Grid to battery (kWh)',
                            'bat (TODO)', 'charge (TODO)', 'eff (TODO)',
                            'Imported energy (kWh)', 'Load (kWh)', 'Exported energy (kWh)',
                            'PV to load (kWh)', 'PV generation (kWh)'])
            for row in energy_data:
                writer.writerow(row)


if __name__ == '__main__':
    main()
